package com.library.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonParser;

public class PageContent extends AbstractModel{

	private final String COLUMN_NAME = "page_contents";
	
	private Page mPage;
	private Format mFormat;
	private String mPageLocation;
	
	private PageContent() {
		setTableName(COLUMN_NAME);
	}
	
	public PageContent(Page page, Format format, String pageLocation) {
		this();
		mPage = page;
		mFormat = format;
		mPageLocation = pageLocation;
	}
	
	public String getFileLocation() {
		return mPageLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.mPageLocation = fileLocation;
	}
	

	public Page getPage() {
		return mPage;
	}

	public void setPage(Page page) {
		this.mPage = page;
	}

	public Format getFormat() {
		return mFormat;
	}

	public void setmFormat(Format Format) {
		this.mFormat = Format;
	}
	
	@Override
	public boolean save() {
		String sql = "";
			
		if(getPage() == null || getFormat() == null) {
			return false;
		}
		
		if(isNew()) {
			sql = "insert into " + getTableName() + "(page_id, format_id, page_location) values(?, ?, ?);";
		} else {
			sql = "update " + getTableName() + " set page_id = ?, format_id = ?, page_location = ?";
		}
		
		System.out.println("SQL: " + sql);
		
		try {
			PreparedStatement prepareStatement = connectionStrategy.getConnection().prepareStatement(sql);
		
			prepareStatement.setInt(1, mPage.getId());
			prepareStatement.setInt(2, mFormat.getId());
			prepareStatement.setString(3, mPageLocation);
			
			prepareStatement.addBatch();
			
			boolean result = prepareStatement.executeBatch().length > 0;
			
			return result;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
		}
	}
	
	/**
	 * 
	 * @param sql query to execute
	 * @param page object to fill it
	 * @return a page object  filled by sql query passed by param.
	 */
	protected static PageContent find(String sql, PageContent pageContent) {
		try {
			ResultSet result = connectionStrategy.executeQuery(sql);
			
			int formatId = 0;
			int pageId = 0;
			
			while(result.next()) {
				
				pageContent.mPageLocation = result.getString("page_location");
	
				formatId = result.getInt("format_id");	
				pageId = result.getInt("page_id");	
				result.close();			
			}
				
			if(formatId > 0) {
				pageContent.setmFormat(Format.find(formatId));
			} else {
				pageContent.setmFormat(null);
			}
			
			if(pageId > 0) {
				pageContent.setPage(Page.find(pageId));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return pageContent;
	}
	
	public static PageContent find(Page page, Format format) {
		return PageContent.find("select * from page_contents where page_id=" + page.getId() + " AND format_id=" + format.getId() + ";", new PageContent());
	}
		
	public static List<PageContent> findAll() {
		List<PageContent> pageContentList = new ArrayList<>();
		List<Integer> pageIds = new ArrayList<>();
		List<Integer> formatIds = new ArrayList<>();
		
		ResultSet result = null;
		
		try {
			result = connectionStrategy.executeQuery("select page_id, format_id from page_contents;");
		
			while(result.next()) {
				pageIds.add(result.getInt(1));
				formatIds.add(result.getInt(2));
			}
			result.close();
			
			
			
			for(int i = 0; i < pageIds.size(); i++) {
				pageContentList.add(PageContent.find(Page.find(pageIds.get(i)), Format.find(formatIds.get(i))));
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageContentList;
	}

	
	@Override
	public boolean destroy() {
		try {
			connectionStrategy.executeUpdate("delete from " + getTableName() + " where id=" + this.id + ";");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}


	@Override
	public String attribute() {
		String formatAttr = "";
		String pageAttr = "";
		
		if(getFormat() != null) {
			formatAttr = (", \"format\" : " + getFormat().attribute() + "");
		}
		
		if(getPage() != null) {
			pageAttr = (", \"page\" : " + getPage().attribute() + "");
		}
		
		return  new JsonParser().parse("{" + "\"id\": " + this.getId() + ", \"PageLocation\": \"" + mPageLocation + "\" }").getAsJsonObject().toString();
	}

}
