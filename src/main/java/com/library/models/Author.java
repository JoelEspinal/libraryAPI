package com.library.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonParser;


public class Author extends AbstractModel{

	private final String COLUMN_AUTHORS = "authors";
		
	private String name = "";
	private String lastName = "";
	
	private Author() {
		setTableName(COLUMN_AUTHORS);
	}
	
	public Author(String name) {
		this();
		setName(name);
	
	}
	
	public Author(String name, String lastName) {
		this(name);
		setLastName(lastName);
	}
	
	
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public boolean save() {
		String sql = "";
		
		if(isNew()) {
			sql = "insert into " + getTableName() + "(name, last_name) values(?, ?);";
		} else {
			sql = "update " + getTableName() + " set name = ?, last_name = ?";
		}
		
		System.out.println("SQL: " + sql);
		
		try {
			PreparedStatement prepareStatement =  connectionStrategy.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, this.name);
			prepareStatement.setString(2, this.lastName);
			prepareStatement.addBatch();
			
			boolean result = prepareStatement.executeBatch().length > 0;
			if(result && isNew())  
					afterSave();
			
			return true;
			} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 
	 * @param sql query to execute
	 * @param author object to fill it
	 * @return an Author object  filled by sql query passed by param.
	 */
	protected static Author find(String sql, Author author) {
		try {
			ResultSet result = connectionStrategy.executeQuery(sql);
			while(result.next()) {
				author.id = result.getInt("id");
				author.name = result.getString("name");
				author.lastName = result.getString("last_name");
				
				result.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return author;
	}
	
	public static Author find(int id) {
		return Author.find("select * from authors where id=" + id, new Author());
	}
	
	public static List<Author> findAll() {
		List<Author> authorList = new ArrayList<>();
		List<Integer> authorIds = new ArrayList<>();
		
		ResultSet result = null;
		
		try {
			result = connectionStrategy.executeQuery("select id from authors;");
		
			while(result.next()) {
				authorIds.add(result.getInt(1));
			}
			result.close();
			
			for(Integer id : authorIds) {
				authorList.add(Author.find(id));
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return authorList;
	}
	
	@Override
	public boolean destroy() {
		try {
			connectionStrategy.executeUpdate("delete from " + getTableName() + " where id=" + this.id + ";");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String attribute() {
		return new JsonParser().parse("{\"id\": " + this.id + ", \"name\": \"" + this.getName() + "\", \"last_name\": \"" +  this.getLastName() + "\" }").getAsJsonObject().toString();
	}

	@Override
	public boolean equals(Object obj) {
		return ((Author) obj).id == this.id;
	}

	
}
