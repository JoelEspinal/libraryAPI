package com.library.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonParser;

public class Format extends AbstractModel{


	private final String TABLE_NAME = "formats";
	
	private String mName;
	private String mExtension;
	
	private Format() {
		setTableName(TABLE_NAME);
	}
	
	public Format(String name, String extension) {
		this();
		this.mName = name;
		this.mExtension = extension;
	}
	
	
	public String getName() {
		return mName;
	}

	public void setmName(String Name) {
		this.mName = Name;
	}

	public String getExtension() {
		return mExtension;
	}

	public void setmExtention(String Extention) {
		this.mExtension = Extention;
	}

	@Override
	public boolean save() {
		String sql = "";
		
		if(isNew()) {
			sql = "insert into " + getTableName() + "(name, extension) values(?, ?);";
		} else {
			sql = "update " + getTableName() + " set name = ?, extension = ?";
		}
		
		System.out.println("SQL: " + sql);
		
		try {
			PreparedStatement prepareStatement =  connectionStrategy.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, this.mName);
			prepareStatement.setString(2, this.mExtension);
			prepareStatement.addBatch();
			
			boolean result = prepareStatement.executeBatch().length > 0;
			if(result && isNew())  
					afterSave();
			
			return true;
			} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 
	 * @param sql query to execute
	 * @param author object to fill it
	 * @return an Author object  filled by sql query passed by param.
	 */
	protected static Format find(String sql, Format format) {
		try {
			ResultSet result = connectionStrategy.executeQuery(sql);
			while(result.next()) {
				format.id = result.getInt("id");
				format.mName = result.getString("name");
				format.mExtension = result.getString("extension");

				result.close();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return format;
	}
	
	
	
	public static Format find(int id) {
		return Format.find("select * from formats where id=" + id, new Format());
	}
	
	public static Format findByExt(String ext) {
		return Format.find("select * from formats where extension='" + ext + "';", new Format());
	}
	
	public static List<Format> findAll() {
		List<Format> formatList = new ArrayList<>();
		List<Integer> formatIds = new ArrayList<>();
		
		ResultSet result = null;
		
		try {
			result = connectionStrategy.executeQuery("select id from formats;");
		
			while(result.next()) {
				formatIds.add(result.getInt(1));
			}
			result.close();
			
			for(Integer id : formatIds) {
				formatList.add(Format.find(id));
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return formatList;
	}
	
	@Override
	public boolean destroy() {
		try {
			connectionStrategy.executeUpdate("delete from " + getTableName() + " where id=" + this.id + ";");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String attribute() {
		// ", \"name\": \"" + this.getName() + ", \"extension\": \"" + this.getExtension() +
		return new JsonParser().parse("{\"id\": " + this.id + ", \"name\": \"" + this.getName() + "\", \"extension\" : \"" + getExtension() + "\" }").getAsJsonObject().toString();
	}

	@Override
	public boolean equals(Object obj) {
		return ((Format) obj).id == this.id;
	}
}
