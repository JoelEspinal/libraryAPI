package com.library.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonParser;


public class Book extends AbstractModel{

	private final String COUMN_BOOKS = "books";
	
	private String mTitle;
	private Author mAuthor;
	
	
	private Book() {
		setTableName(COUMN_BOOKS);
	}
	
	public Book(Author author) {
		this();
		mAuthor = author;
	}
	
	public Book(Author author, String title) {
		this(author);
		mTitle = title;
	}
	
	public String getTitle() {
		return mTitle;
	}

	public void setTitle(String mTitle) {
		this.mTitle = mTitle;
	}

	public Author getAuthor() {
		return mAuthor;
	}

	public void setAuthor(Author author) {
		this.mAuthor = author;
	}

	@Override
	public boolean save() {
	String sql = "";
		
		if(isNew()) {
			sql = "insert into " + getTableName() + "(title, author_id) values(?, ?);";
		} else {
			sql = "update " + getTableName() + " set title = ?, author_id = ?";
		}
		
		System.out.println("SQL: " + sql);
		
		try {
			PreparedStatement prepareStatement =  connectionStrategy.getConnection().prepareStatement(sql);
			prepareStatement.setString(1, this.getTitle());
			if(getAuthor() != null) {
				prepareStatement.setInt(2, this.getAuthor().getId());
			} else {
				prepareStatement.setInt(2, 0);
			}
			prepareStatement.addBatch();
			
			boolean result = prepareStatement.executeBatch().length > 0;
			if(result && isNew())  
					afterSave();
			
			return true;
			} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * 
	 * @param sql query to execute
	 * @param book object to fill it
	 * @return a Book object  filled by sql query passed by param.
	 */
	protected static Book find(String sql, Book book) {
		try {
			ResultSet result = connectionStrategy.executeQuery(sql);
			
			while(result.next()) {
				book.id = result.getInt("id");
				book.mTitle = result.getString("title");
				
				int authorId = result.getInt("author_id");
				if(authorId > 0) {
					book.setAuthor(Author.find(authorId));
				} else {
					book.setAuthor(null);
				}
				
				result.close();
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return book;
	}
	
	public static Book find(int id) {
		return Book.find("select * from books where id=" + id + ";", new Book());
	}
	
	public static List<Book> findAll() {
		List<Book> bookList = new ArrayList<>();
		List<Integer> bookIds = new ArrayList<>();
		
		ResultSet result = null;
		
		try {
			result = connectionStrategy.executeQuery("select id from books;");
		
			while(result.next()) {
				bookIds.add(result.getInt(1));
			}
			result.close();
			
			for(Integer id : bookIds) {
				bookList.add(Book.find(id));
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bookList;
	}


	@Override
	public boolean destroy() {
		try {
			connectionStrategy.executeUpdate("delete from " + getTableName() + " where id=" + this.id + ";");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String attribute() {
		String authorAttr = "";
		if(getAuthor() != null) {
			authorAttr = (", \"author\" : " +  this.getAuthor().attribute() + "");
		}
		
		return new JsonParser().parse("{\"id\": " + this.id + ", \"title\": \"" + this.getTitle() + "\"" + authorAttr + " }").getAsJsonObject().toString();
	}

	@Override
	public boolean equals(Object obj) {
		return ((Book) obj).id == this.id;
	}
}
