package com.library.models;

import java.sql.ResultSet;

import com.library.connections.AbstractConnection;
import com.library.main.Main;

public abstract class AbstractModel implements Model {

	protected int id = 0;
	protected String tableName = "";
	protected static AbstractConnection connectionStrategy = (AbstractConnection) Main.sConnecionContex.getConnectionStrategy();
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public boolean isNew() {
			return id == 0;
	}
	
	protected void setTableName(String columnName) {
		this.tableName = columnName;
	}
	
	protected String getTableName() {
		return this.tableName;
	}
	
	
	@Override
	public void afterSave() {
		try{
			ResultSet result = connectionStrategy.executeQuery("select max(id) from " + getTableName());
			if(result != null) id = result.getInt(1);			
		}catch(Exception e){e.printStackTrace();}	
	}
	
	@Override
	public int hashCode() {
		return getId();
	}
	
	@Override
	public String toString() {
		return attribute();
	}
}
