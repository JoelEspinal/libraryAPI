package com.library.models;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonParser;

public class Page extends AbstractModel{


	private final String TABLE_NAME = "pages";
	private int mPageNumber;
	private Book mBook;
	
	private Page() {
		setTableName(TABLE_NAME);
	}
	
	public Page(Book book) {
		this();
		mBook = book;
	}
		
	public Page(Book book, int pageNumber) {
		this(book);
		mPageNumber = pageNumber;
	}

	public int getPageNumber() {
		return mPageNumber;
	}

	public void setPageNumber(int PageNumber) {
		this.mPageNumber = PageNumber;
	}

	public Book getBook() {
		return mBook;
	}

	public void setBook(Book mBook) {
		this.mBook = mBook;
	}


	@Override
	public boolean save() {
		String sql = "";
			
		if(isNew()) {
			sql = "insert into " + getTableName() + "(page_number, book_id) values(?, ?);";
		} else {
			sql = "update " + getTableName() + " set page_number = ?, book_id = ?";
		}
		
		System.out.println("SQL: " + sql);
		
		try {
			PreparedStatement prepareStatement =  connectionStrategy.getConnection().prepareStatement(sql);
			prepareStatement.setInt(1, this.getPageNumber());
			
			if(getBook() != null) {
				prepareStatement.setInt(2, this.getBook().getId());
			} else {
				prepareStatement.setInt(2, 0);
			}
			prepareStatement.addBatch();
			
			boolean result = prepareStatement.executeBatch().length > 0;
			if(result && isNew())  
					afterSave();
			
			return true;
			} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 
	 * @param sql query to execute
	 * @param page object to fill it
	 * @return a page object  filled by sql query passed by param.
	 */
	protected static Page find(String sql, Page page) {
		try {
			ResultSet result = connectionStrategy.executeQuery(sql);
			int bookId = 0;
			while(result.next()) {
				page.id = result.getInt("id");	
				page.mPageNumber = result.getInt("page_number");			
			
				bookId = result.getInt("book_id");
				
				result.close();			
			}
			
			
			if(bookId > 0) {
				System.out.println("Book Id: " + bookId);
				result.close();
				page.setBook(Book.find(bookId));
			} else {
				page.setBook(null);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return page;
	}
	
	public static Page find(int id) {
		return Page.find("select * from pages where id=" + id, new Page());
	}
	
	public static Page find(Book book, int pageNumber) {
		return Page.find("select * from pages where book_id=" + book.getId() + " and page_number=" + pageNumber + ";", new Page());
	}
	
	public static List<Page> findAll() {
		List<Page> pageList = new ArrayList<>();
		List<Integer> pageIds = new ArrayList<>();
		
		ResultSet result = null;
		
		try {
			result = connectionStrategy.executeQuery("select id from pages;");
		
			while(result.next()) {
				pageIds.add(result.getInt(1));
			}
			result.close();
			
			for(Integer id : pageIds) {
				pageList.add(Page.find(id));
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pageList;
	}


	@Override
	public boolean destroy() {
		try {
			connectionStrategy.executeUpdate("delete from " + getTableName() + " where id=" + this.id + ";");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public String attribute() {
		String bookAttr = "";
		if(getBook() != null) {
			bookAttr = (", \"book\" : " +  this.getBook().attribute() + "");
		}
		// "{" + "\"id\": " + this.getId() + "\", page_number\": " + getPageNumber()  + "\", book\": " + bookAttr +"}"
		return new JsonParser().parse("{" + "\"id\": " + this.getId() + ", \"page_number\" : " + getPageNumber() + bookAttr + "}").getAsJsonObject().toString();
	}

	@Override
	public boolean equals(Object obj) {
		return ((Page) obj).id == this.id;
	}
}
