package com.library.models;

public interface Model {
	
	public boolean save();
	public boolean destroy();
	public String attribute();
	public void afterSave();
	
}
