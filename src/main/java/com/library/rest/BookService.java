package com.library.rest;
 
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.library.main.Main;
import com.library.models.Book;
import com.library.models.Format;
import com.library.models.Page;
import com.library.models.PageContent;
 
@Path("/")
public class BookService {
 
	@GET
	@Path("books")
	@Produces({MediaType.APPLICATION_JSON})
	public Response getBooks() {
		
		List<Book> bookList = Book.findAll();
		List<String> responseString = new ArrayList<>();
		
		for(Book book : bookList) {
			responseString.add(book.attribute());
		}
		return Response.status(200).entity(responseString.toString()).build();
	}
	
	@GET
	@Path("book/{id}")
	public Response getBook(@PathParam("id") int id) {
 
		Book book = Book.find(id);
		return Response.status(200).entity(book.attribute()).build();
	}
	
	@GET
	@Path("book/{title}/{page}")
	public Response getPage(@PathParam("title") String title, @PathParam("page") int page) {
		String output = "title :" +  title + "/ page: " + page;
		return Response.status(200).entity(output).build();
	}
 
	@GET
	@Path("book/{id}/{page}/{format}")
	public Response getPage(@PathParam("id") int id, @PathParam("page") int page, @PathParam("format") String format) {
		
		if(Book.find(id).getId() == 0) {
			return Response.status(404).entity("Book not found").build();
		}
		
		System.out.println("Format " + Format.findByExt(format));
		
		PageContent pageContent = PageContent.find(Page.find(Book.find(id), page), Format.findByExt(format));
		
		String fileLocation;		
		if(pageContent == null) {
			return Response.status(404).entity("Page not found in requested format").build();
		}
		
		fileLocation = pageContent.getFileLocation();

		if(fileLocation == null || fileLocation.isEmpty()) {
			return Response.status(404).entity("Page not found in requested format").build();
		}
		
		if(fileLocation.startsWith("http")) {
			try {
				/**
				 * info: https://stackoverflow.com/questions/11116687/redirecting-to-a-page-using-restful-methods
				 */
				
				return Response.temporaryRedirect(new URI(fileLocation)).build();
			} catch (URISyntaxException e) {
				return Response.status(404).entity("Resource not found in external service").build();
			}
		}
		else {
			
			System.out.println("File location:" + fileLocation);
			
			URL pageUrl = Main.class.getClassLoader().getResource(fileLocation); //"books/" + id + "/" + format + "/" + page + "." + format);
			ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			
			InputStream inputStream;
			try {
				inputStream = pageUrl.openStream();
				
				int read = inputStream.read();
				while (read != -1) {
					byteArrayOutputStream.write(read);
					read = inputStream.read();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			byte[] bytes = byteArrayOutputStream.toByteArray();
			
			return Response.status(200).entity(bytes).build();
		}

	}
}