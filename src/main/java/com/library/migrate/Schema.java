package com.library.migrate;

import java.util.HashMap;

public class Schema extends HashMap<String, String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Schema() {
		
	}
	
	public void initialize() {
		
		put("Authors",
					"CREATE TABLE IF NOT EXISTS authors ("
					+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "name VARCHAR(50),"
					+ "last_name VARCHAR(50)"
					+ ");");
		
		put("Books", 
					"CREATE TABLE IF NOT EXISTS books ("
					+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "title VARCHAR(50) NOT NULL,"
					+ "str_title VARCHAR(255),"
					+ "author_id INTEGER,"
					+ "FOREIGN KEY (author_id) REFERENCES authors(id) ON DELETE CASCADE"
					+ ");");
		
		put("Pages", 
				"CREATE TABLE IF NOT EXISTS pages ("
				+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
				+ "page_number INTEGER NOT NULL,"
				+ "book_id INTEGER,"
				+ "FOREIGN KEY (book_id) REFERENCES books(id) ON DELETE CASCADE"
				+ ");");
	
		put("Formats", 
					"CREATE TABLE IF NOT EXISTS formats ("
					+ "id INTEGER PRIMARY KEY AUTOINCREMENT,"
					+ "name VARCHAR(15) NOT NULL,"
					+ "extension VARCHAR(15) NOT NULL"
					+ ");");
		
		put("Page Contents", 
				"CREATE TABLE IF NOT EXISTS page_contents ("
				+ "page_id INTEGER,"
				+ "format_id INTEGER,"
				+ "page_location VARCHAR(255) NOT NULL,"
				+ "FOREIGN KEY (page_id) REFERENCES pages(id),"
				+ "FOREIGN KEY (format_id) REFERENCES formats(id) ON DELETE CASCADE,"
				+ "PRIMARY KEY (page_id, format_id)"
				+ ");");
	
	}
}
