package com.library.migrate;

import java.util.ArrayList;
import java.util.List;

import com.library.main.Main;
import com.library.models.Author;
import com.library.models.Book;
import com.library.models.Format;
import com.library.models.Page;
import com.library.models.PageContent;

public class Seed {

	private Seed() {
		
	}
	
	public static void createTables() {
		try {
			Schema schema = new Schema();
			schema.initialize();
			Main.sConnecionContex.getConnectionStrategy().executeUpdate(new ArrayList<>(schema.values()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public static void fillDatabase() {

		List<Format> formatList = new ArrayList<>();
		
		Format plainText = new Format("plain text", "txt");
		formatList.add(plainText);
		
		Format html = new Format("HTML", "html");
		formatList.add(html);
		
		for(Format format : formatList) {
			format.save();
		}
		
		// --------------------------
		List<Author> authorList = new ArrayList<>();
		
		Author antoine = new Author("Antoine", "de Saint-Exup�ry");
		authorList.add(antoine);
		
		Author eliax = new Author("Jose", "Elias");
		authorList.add(eliax);
		
		for(Author author : authorList) {
			System.out.println("Saved:  " + author.save());
		}
		
		// ---------------------------
		List<Book> bookList = new ArrayList<>();
		
		Book lePettitPrince = new Book(antoine, "le petit prince");
		bookList.add(lePettitPrince);
		
		Book maquinasEnElParaiso = new Book(eliax, "Maquinas en el paraiso");
		bookList.add(maquinasEnElParaiso);

		for(Book book : bookList) {
			System.out.println("Saved:  " + book.save());
		}
		
		// ---------------------------
		List<Page> lePettitPrincePages = new ArrayList<>();
		List<Page> maquinasEnElParaisoPages = new ArrayList<>();
		
		for(int i = 1; i <= 10; i++) {
			Page page = new Page(lePettitPrince, i);
			lePettitPrincePages.add(page);
			System.out.println("Saved:  " + page.save());
			
			page = new Page(maquinasEnElParaiso, i);
			maquinasEnElParaisoPages.add(page);
			System.out.println("Saved:  " + page.save());
		}
		
		// ---------------------------
		List<PageContent> pageContentList = new ArrayList<>();

			// html
		for(Page page : lePettitPrincePages) {
			PageContent content = new PageContent(page, html, "books/" + lePettitPrince.getId() + "/"  + html.getExtension() + "/" + page.getPageNumber() + "." + html.getExtension());
			pageContentList.add(content);
			
			content = new PageContent(page, plainText, "books/" + lePettitPrince.getId() + "/"  + plainText.getExtension() + "/" + page.getPageNumber() + "." + plainText.getExtension());
			pageContentList.add(content);
		}
		
		for(Page page : maquinasEnElParaisoPages) {			
			PageContent content = new PageContent(page, plainText, "books/" + maquinasEnElParaiso.getId() + "/"  + plainText.getExtension() + "/" + page.getPageNumber() + "." + plainText.getExtension());
			pageContentList.add(content);
		}
		
		for(PageContent pageContent : pageContentList) {
			System.out.println("Saved:  " + pageContent.save());
		}	
		
	}
}
