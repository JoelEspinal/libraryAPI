package com.library.main;


import java.io.File;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.library.connections.ConnectionContext;
import com.library.connections.SQLiteConnectionStrategy;
import com.library.migrate.Seed;

public class Main implements ServletContextListener  {

	public static ConnectionContext sConnecionContex = new ConnectionContext();
	SQLiteConnectionStrategy sqliteStrategy = SQLiteConnectionStrategy.getInstance();
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		System.out.println(sce.getServletContext().getRealPath("/"));
		

		System.out.println("** --  initializing Connections  -- **");

		SQLiteConnectionStrategy sqliteStrategy = SQLiteConnectionStrategy.getInstance();
		sqliteStrategy.setDbPath(sce.getServletContext().getRealPath("/"));
		sqliteStrategy.setDbName("/library.db");
		
		sConnecionContex.setConnectionStrategy(sqliteStrategy);
		
		System.out.println("** --  create database tables  -- **");
		Seed.createTables();
		System.out.println("** --  Seeding database  -- **");
		Seed.fillDatabase();
		System.out.println("** --  End Seding  -- **");
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		// Remove Database
		
		try {
				sConnecionContex.getConnectionStrategy().closeConnection();
				boolean isDeleted = new File(sqliteStrategy.getDbPath() + sqliteStrategy.getDbName()).delete();
				System.out.println("Database deleted: " + isDeleted);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
}
