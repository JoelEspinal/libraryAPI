package com.library.connections;

public class ConnectionContext {
	
	private AbstractConnection mConnectionStrategy;
	
	public ConnectionContext() {
		
	}
	
	public void setConnectionStrategy(AbstractConnection connection) {
		mConnectionStrategy = connection;
	}

	public AbstractConnection getConnectionStrategy() {
		return mConnectionStrategy;
	}
}
