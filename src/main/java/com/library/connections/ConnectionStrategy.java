package com.library.connections;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;

public interface ConnectionStrategy {

	public Connection getConnection()throws Exception;
	public ResultSet executeQuery(String sql) throws Exception;
	public int executeUpdate(String sql) throws Exception;
	public boolean executeUpdate(List<String> sql) throws Exception;
	
}
