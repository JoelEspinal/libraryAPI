package com.library.connections;

import java.sql.Connection;
import java.sql.SQLException;

public abstract class AbstractConnection implements ConnectionStrategy {

	protected String dbPath = "";
	protected String dbName = "";
	protected static Connection connection;
	
	public void closeConnection() {
		if(connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public String getDbPath() {
		return dbPath;
	}

	public void setDbPath(String dbPath) {
		this.dbPath = dbPath;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	
}
