package com.library.connections;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

public class SQLiteConnectionStrategy extends AbstractConnection {

	private static SQLiteConnectionStrategy instance;
	
	
	private SQLiteConnectionStrategy() {
		
	}
	
	public static SQLiteConnectionStrategy getInstance() {
		if(instance == null) {
			instance = new SQLiteConnectionStrategy();
		}
		
		return instance;
	}
	

	@Override
	public Connection getConnection() throws Exception {
		closeConnection();
		
		Class.forName("org.sqlite.JDBC");
		connection = DriverManager.getConnection("jdbc:sqlite:" + getDbPath() + getDbName());
		
		return connection;
	}
	
	@Override
	public int executeUpdate(String sql) throws Exception {
		Statement statement = getConnection().createStatement();
		int result = statement.executeUpdate(sql);
		
		if(statement != null) {
			statement.close();
		}
		return result;
	}

	@Override
	public ResultSet executeQuery(String sql) throws Exception {
		return getConnection().createStatement().executeQuery(sql);
	}

	public boolean executeUpdate(List<String> sql)throws Exception{
		System.out.println(sql);
		Statement statement = getConnection().createStatement();
		for(String s : sql){
			statement.addBatch(s);			
		}
		statement.executeBatch();
		
		if(statement != null) {
			statement.close();
		}
		
		return true;
	}

}
